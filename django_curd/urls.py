"""again URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from curd import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # curd
    url(r'^add', views.Add.as_view(),name='add'),
    url(r'^list$', views.Listing.as_view(), name="list"),
    url(r'^edit/(?P<pk>\w+)$', views.Edit.as_view(),name='edit'),
    url(r'^delete/(?P<pk>\w+)$', views.Destroy.as_view(),name='delete'),

    # file operation
    url(r'^upload_files$', views.UploadFile.as_view(),name='upload_files'),
    url(r'^listing_files$', views.FileListing.as_view(),name='listing_files'),
    url(r'^download_zip$', views.DownloadZip.as_view(),name='download_zip'),
    url(r'^create_zip$', views.create_zip, name='zip'),

    # sftp,ssh telnet



]
