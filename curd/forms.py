from django import forms
from curd.models import Telnet
from django.conf import settings


class TelnetForm(forms.ModelForm):
    id = forms.IntegerField(required=True)

    def clean_ename(self):
        return self.cleaned_data["ename"].strip()

    def save(self):
        Telnet.objects.filter(id=self.cleaned_data['id']).update(
            ename=self.cleaned_data['ename'],
            eemail=self.cleaned_data['eemail'],
            econtact=self.cleaned_data['econtact']
        )

    class Meta:
        model = Telnet
        fields = "__all__"

class UploadForm(forms.Form):
    title = forms.CharField(max_length=50)
    myfile = forms.FileField()

    def upload_file(self,file_data):
        with open(settings.MEDIA_ROOT + "/"+ file_data.name, 'wb+') as destination:
            for chunk in file_data.chunks():
                destination.write(chunk)
