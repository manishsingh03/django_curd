from django.shortcuts import render, redirect
from django.views.generic import ListView, CreateView, DeleteView, FormView,View
from django.core.urlresolvers import reverse_lazy
from django.http import JsonResponse
from django.http import HttpResponse
from curd.forms import TelnetForm, UploadForm
from curd.models import Telnet
from zipfile import ZipFile
import os
from django.core.files import File
from django.conf import settings
import logging
logger = logging.getLogger(__name__)


class Add(CreateView):
    template_name = "index.html"
    form_class = TelnetForm
    http_method_names = ['get', 'post']
    success_url = reverse_lazy("list")

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        add_url = reverse_lazy("add")
        context_data["add_url"] = add_url
        return context_data

    def form_valid(self, form):
        self.object = form.save()
        return redirect(reverse_lazy('list'))

    def form_invalid(self, form):
        error = form.errors.get("__all__")
        if not error:
            error = form.errors
        return JsonResponse({'error': error}, status=400)

class Listing(ListView):
    model = Telnet
    template_name = 'show.html'
    http_method_names = ['get']

    # def get_queryset(self):
        # return self.queryset.filter(creator_id=self.request.user.id).order_by('-time_created'). \
        #     order_by('-last_updated')


    def get_context_data(self, **kwargs):
        context_data = {
            'employees':  Telnet.objects.all()
        }
        return context_data


class Edit(FormView):
    form_class = TelnetForm
    success_url = reverse_lazy("list_intents")
    template_name = 'edit.html'
    http_method_names = ['get', 'post']

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data['employee'] = Telnet.objects.get(id=self.kwargs.get('pk'))
        edit_url = reverse_lazy("edit", kwargs={"pk": self.kwargs['pk']})
        context_data["edit_url"] = edit_url
        return context_data

    def get_initial(self):
        """
        Returns the initial data to use for forms on this view.
        """
        initial = super().get_initial()
        id = self.kwargs["pk"]
        return initial

    def form_valid(self, form):
        self.object = form.save()
        return redirect(reverse_lazy('list'))

    def form_invalid(self, form):
        error = form.errors.get("__all__")
        if not error:
            error = form.errors
        return JsonResponse({'error': error}, status=400)


class Destroy(DeleteView):
    queryset = Telnet.objects.all()
    http_method_names = ['delete']
    success_url = reverse_lazy('list')

    def delete(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        telnet_obj = Telnet.objects.get(id=pk)
        telnet_obj.delete()
        return JsonResponse({'success': "Delete successfully"}, status=200)





class UploadFile(FormView):
    template_name = "upload.html"
    form_class = UploadForm
    http_method_names = ['get', 'post']
    success_url = reverse_lazy("listing_files")


    def form_valid(self, form):
        form.upload_file(form.files['myfile'])
        return redirect(reverse_lazy('upload_files'))

    def form_invalid(self, form):
        error = form.errors.get("__all__")
        if not error:
            error = form.errors
        return JsonResponse({'error': error}, status=400)



class FileListing(View):

    def get(self,request):
        context_data = {
            'file_listing':  os.listdir(settings.MEDIA_ROOT)
        }
        return render(request, 'file_listing.html', context_data)


class DownloadZip(View):

    def get(self, request):
        download_report_link = '/home/manish/backup/manish/workspace/again/media/manage.py'
        f = open(download_report_link, 'rb')
        file_obj = File(f)
        response = HttpResponse(file_obj, content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename=' + "UserResponsesReport.py"
        response['Content-length'] = file_obj.size
        os.remove(download_report_link)
        return response



def create_zip(request):
    filepath = '/home/manish/backup/manish/workspace/again/test.zip'
    with ZipFile(filepath, 'w') as zipf:
        # Add a file located at the source_path to the destination within the zip
        # file. It will overwrite existing files if the names collide, but it
        # will give a warning
        source_path = '/home/manish/backup/manish/workspace/again/manage.py'
        destination = 'manage.py'
        zipf.write(source_path, destination)

        zipf.printdir()

        # extracting all the files
        print('Extracting all the files now...')
        zipf.extractall()

    return JsonResponse({}, status=200)




