import logging
from ftplib import FTP
import paramiko
logger = logging.getLogger(__name__)
from django.conf import settings

class FileSharing(object):

    def excute(self,local_file, remote_file, remote_path, username, password, server_ip, port, mode='sftp'):
        sharing_status = False
        error_message = ""
        if not remote_path.endswith("/"):
            remote_path += "/"
        if mode == 'ftp':
            sharing_status, error_message = self.file_sharing_using_ftp(
                local_file,
                remote_file,
                username,
                password,
                server_ip,
                port,
                remote_path
            )
        elif mode == 'sftp':
            sharing_status, error_message = self.file_sharing_using_sftp(
                local_file,
                remote_file,
                username,
                password,
                server_ip,
                port,
                remote_path
            )
        return sharing_status, error_message

    def file_sharing_using_ftp(self,
            local_file,
            remote_file,
            user_name,
            password,
            server_ip,
            port=21,
            remote_path='/',
    ):
        status = False
        error_message = ''
        fp = None
        try:
            ftp = FTP()
            ftp.set_debuglevel(2)
            ftp.set_pasv(settings.FILE_SHARING_PASSIVE_MODE)
            ftp.connect(server_ip, port)
            ftp.login(user_name, password)
            # it create folder and change path
            self.ftp_create_dir(ftp, remote_path)
            ftp.cwd(remote_path)
            fp = open(local_file, 'rb')
            ftp.storbinary('STOR %s' % remote_file, fp, 1024)
            logger.info("File transfer Successfully with local file: %s, remote file: %s and service mode is: FTP",
                        local_file, remote_file)
            status = True
        except Exception as e:
            error_message = str(e)
            logger.exception("Exception when make ftp connection: {}".format(error_message))
        finally:
            try:
                if fp:
                    fp.close()
            except Exception as e:
                error_message += str(e)
                logger.exception("Exception when close ftp connection: {}".format(error_message))
            return status, error_message

    def file_sharing_using_sftp(
            self,
            local_file,
            remote_file,
            user_name,
            password,
            server_ip,
            port=22,
            remote_path='/',
            sftp_key_status=True
    ):
        status = False
        error_message = ''
        sftp = None
        try:
            config_data = {
                "host": server_ip,
                "username": user_name,
                "port": port
            }
            if password:
                config_data['password'] = password
            if sftp_key_status:
                config_data['sftp_key'] = settings.KEY_FILE_NAME
            sftp, trans = self.sftp_connection(**config_data)
            self.mkdir_recursive(sftp, remote_path)
            sftp.put(local_file, remote_path + remote_file)
            logger.info("File transfer Successfully with local file: %s, remote file: %s and service mode is: SFTP",
                        local_file, remote_file)
            status = True
        except Exception as exe:
            error_message = str(exe)
            logger.exception("SFTP file sharing Exception: {}".format(error_message))
        finally:
            try:
                if sftp:
                    sftp.close()
            except Exception as exe:
                error_message += str(exe)
                logger.exception("Close sftp connection Exception: {}".format(str(exe)))
            return status, error_message

    @staticmethod
    def ftp_create_dir(ftp, dir):
        if FileSharing.directory_exists(ftp, dir) is False:
            try:
                ftp.mkd(dir)
            except Exception as exe:
                logger.info("Directory exist in ftp server {}  and exception is {}".format(dir, str(exe)))
        ftp.cwd(dir)

    @staticmethod
    def directory_exists(ftp, dir):
        filelist = []
        ftp.retrlines('LIST', filelist.append)
        for f in filelist:
            if f.split()[-1] == dir and f.upper().startswith('D'):
                return True
        return False

    @staticmethod
    def sftp_connection(self, host=None, port=None, username=None, password=None, sftp_key=None):
        transport = paramiko.Transport((host, port))
        # if user name, password and sftp key all are
        if password and sftp_key:
            sftp_key = paramiko.RSAKey.from_private_key_file(sftp_key)
            transport.start_client(event=None, timeout=15)
            transport.get_remote_server_key()
            transport.auth_publickey(username, sftp_key, event=None)
            transport.auth_password(username, password, event=None)
        # if only username and sftp key but not password
        elif not password and sftp_key:
            sftp_key = paramiko.RSAKey.from_private_key_file(sftp_key)
            transport.connect(username=username, pkey=sftp_key)
        # if key not exist only user name and password
        elif username and password and not sftp_key:
            transport.connect(username=username, password=password)
        else:
            error_message = "No matching condition found. host {} password {} username{} sftp_key{}".format(host, password, username, sftp_key)
            logger.info(error_message)
            raise Exception(error_message)
        sftp = paramiko.SFTPClient.from_transport(transport)
        return sftp, transport

    @staticmethod
    def mkdir_recursive(sftp, remote_directory):
        dir_path = str()
        for dir_folder in remote_directory.split("/"):
            if dir_folder == "":
                continue
            dir_path += r"/{0}".format(dir_folder)
            try:
                sftp.listdir(dir_path)
            except IOError:
                sftp.mkdir(dir_path)
